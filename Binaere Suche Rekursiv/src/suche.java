
import java.util.Arrays;
import java.util.Random;

public class suche  {
  
  private final int ANZAHL  = 100;
  private final int MAXZAHL = (10*ANZAHL);
      
   
  private int zahlen[] = new int[ANZAHL];
    
   
  private Random zufall = new Random();
    

  public suche() {
    for (int i=0; i<ANZAHL; i++)
      zahlen[i] = 0;
  }

  public int[] liefereZahlen() {
    return zahlen;
  }

  public void ermittleZahlen() {
    for (int i=0; i<ANZAHL ;i++){
      boolean vorhanden = false;
      zahlen[i] = zufall.nextInt(MAXZAHL) + 1; 
      
    }
    Arrays.sort(zahlen);
  }
       
   
  
  public int binaereSucheRek(int zahl, int von, int bis){
    if (von <= bis) {
      int trennindex = (von + bis) / 2;
      if (zahl < zahlen[trennindex]) {
        return binaereSucheRek(zahl, von, trennindex -1);
      } // end of if
      else {
        if (zahl > zahlen[trennindex]) {
          return binaereSucheRek(zahl, trennindex +1, bis);
        } else {
          return trennindex;
        } 
      } 
    } else {
      return -1;
    }
  }    
}