package oszkickers;

public class Spieler extends Mitglied
{
	// Variables
	private int trikonummer;
	private String spielposition;

	// Constructor
	public Spieler(String name_new, String tel_new, boolean jahr_new, int triko_new, String spielpos_new) 
	{
		super(name_new, tel_new, jahr_new);
		
		trikonummer = triko_new;
		spielposition = spielpos_new;
	}

	// Methods
	public int getTrikonummer() 
	{
		return trikonummer;
	}

	public void setTrikonummer(int triko_new) 
	{
		this.trikonummer = triko_new;
	}

	public String getSpielposition() 
	{
		return spielposition;
	}

	public void setSpielposition(String spielpos_new) 
	{
		this.spielposition = spielpos_new;
	}
	
}
