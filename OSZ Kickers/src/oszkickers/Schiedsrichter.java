package oszkickers;

public class Schiedsrichter extends Mitglied
{
	// Variables
	private int gepfiffeneSpieler;
	
	// Constructor
	public Schiedsrichter(String name_new, String tel_new, boolean jahr_new, int gepfSpieler_new) 
	{
		super(name_new, tel_new, jahr_new);

		gepfiffeneSpieler = gepfSpieler_new;
	}

	// Methods
	public int getGepfiffeneSpieler() 
	{
		return gepfiffeneSpieler;
	}

	public void setGepfiffeneSpieler(int gepfSpieler_new) 
	{
		this.gepfiffeneSpieler = gepfSpieler_new;
	}
}
