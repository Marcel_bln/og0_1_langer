package oszkickers;

public class Mannschaftsleiter extends Mitglied
{
	// Variables
	private double rabatt;
	
	// Constructor
	public Mannschaftsleiter(String name_new, String tel_new, boolean jahr_new, double rabatt_new) 
	{
		super(name_new, tel_new, jahr_new);

		rabatt = rabatt_new;
	}

	//hallo
	// Methods
	public double getRabatt() 
	{
		return rabatt;
	}

	public void setRabatt(double rabatt)
	{
		this.rabatt = rabatt;
	}
}
