package de.oszimt.spaeti;

public class Lieferant {

	private String name;
	private String ort;
	
	public Lieferant() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}


}
