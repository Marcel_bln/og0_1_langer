import java.util.*;

public class PersonTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Person p1 = new Person("p2003", "Dennis");
		Person p2 = new Person("p2004", "Marcel");
		Person p3 = p1;

		System.out.println("p1: " + p1);
		System.out.println("p2: " + p2);
		System.out.println("p3: " + p3);

		System.out.println("");

		System.out.println("p1.equals(p2)--> " + p1.equals(p2));
		System.out.println("p1.equals(p3)--> " + p1.equals(p3));

		List<Person> pliste = new ArrayList<Person>();

		pliste.add(p2);
		pliste.add(p1);

		System.out.println(pliste);

		pliste.sort(null);

		System.out.println(pliste);

	}

}
