
public class Primzahlen {

	public static void main(String[] args) {
		long limit = Long.MAX_VALUE;
		long zahl;
		long zaehler;
		boolean primzahl;

		for (zahl = 2; zahl <= limit; zahl++) {

			primzahl = true;

			for (zaehler = 2; zaehler < Math.sqrt(zahl) + 1; zaehler++) {
				if (zahl % zaehler == 0) {

					primzahl = false;
					break;
				}
			}
		}
	} 
}
