package rekursion;

public class MiniMath {

	public static int berechneFakultaet(int n) {
		if (n == 0 || n == 1) {
			return 1;

		} else {
			return n * berechneFakultaet(n-1);
			}
		}

	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * 
	 * @param n
	 *            - Angabe der Potenz (max. 31 sonst Integeroverflow)
	 * @return 2^n
	 */
	public static int berechneZweiHoch(int n) {
		
		return 1;
	} else {
		return 2 * berechnerzweiHoch(n - 1);
	}

	/**
	 * Die Methode berechnet die Summe der Zahlen von 1 bis n (also 1+2+...+(n-1)+n)
	 * 
	 * @param n
	 *            - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static int berechneSumme(int n) {
		return 0;
	}

}
